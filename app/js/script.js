const btnHamburger = document.querySelector("#btnHamburger");
const body = document.querySelector('.noscroll');
const header = document.querySelector(".header")
const fadeElem = document.querySelectorAll(".has_fade");
const overlay = document.querySelector(".overlay");


btnHamburger.addEventListener('click',function(){
    console.log("click hamburger");

    if (header.classList.contains('open')){
        body.classList.remove('noscroll');
        header.classList.remove('open');

        overlay.classList.add("fade-out");
        overlay.classList.remove('fade-in')

        fadeElem.forEach(function(element){
            element.classList.add("has_fade");
            element.classList.remove("fade-in");
            element.classList.add("fade-out");
        })
    
    }else{ //open hamburger menu
        body.classList.add("noscroll");
        header.classList.add('open');
        fadeElem.forEach(function(element){
            element.classList.add("fade-in");
            element.classList.remove("has_fade");
            element.classList.remove("fade-out");
        })
    }
  
});